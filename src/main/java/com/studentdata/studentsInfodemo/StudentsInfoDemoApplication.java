package com.studentdata.studentsInfodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentsInfoDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentsInfoDemoApplication.class, args);
	}

}
