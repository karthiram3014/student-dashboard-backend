package com.studentdata.studentsInfodemo.controller;

import com.studentdata.studentsInfodemo.model.Task;
import com.studentdata.studentsInfodemo.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;



@CrossOrigin(origins="*",allowedHeaders = "*")
@RestController
public class TaskController {
    @Autowired
    private TaskService service;

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public Task createTask(@RequestBody Task task) {
        return service.addTask(task);
    }

    @GetMapping("/get")
    public List<Task> getTasks() {
        return service.findAllTasks();
    }

    @GetMapping("/get/{id}")
    public Task getTask(@PathVariable String id) {
        return service.getTaskById(id);

    }

    @GetMapping("/name/{name}")
    public List<Task> getTasks(@PathVariable String name) {
        return service.getTaskByName(name);
    }

    @PutMapping("/update")
    public Task modifyTask(@RequestBody Task task) {
        return service.updateTask(task);
    }

    @PostMapping("/login")
    public boolean login(@RequestParam("userId") String userId, @RequestParam("password") String password) {
      System.out.println("hi");
            return service.checkUser(userId, password);


    }
/*
    @DeleteMapping("/{id}")
    public String deleteTask(@PathVariable String id) {
        System.out.println(id);
        return service.deleteTask(id);
    }*/

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteStudent(@PathVariable("id") String id) {
        service.deleteStudent(String.valueOf(id));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}