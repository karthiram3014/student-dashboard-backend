package com.studentdata.studentsInfodemo.model;


import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="students")
@Data
public class Task {

    @Id
    private String id;
    private String name;
    private String email;
    //private String password;
    private String address;
     private String  fname;
    private String  tname;
    private int standard;
    private int percentage;
    private String phone;



}
