package com.studentdata.studentsInfodemo.repository;

import com.studentdata.studentsInfodemo.model.Task;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface TaskRepository extends MongoRepository <Task,String>{
    List<Task> findByName(String name);



    @Query("{email: ?0 }")
    List<Task> getTaskByEmail(String email);
}
