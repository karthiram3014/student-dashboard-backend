package com.studentdata.studentsInfodemo.service;


import com.studentdata.studentsInfodemo.model.Task;
import com.studentdata.studentsInfodemo.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.UUID;


@Service
public class TaskService {

    @Autowired
    private TaskRepository repository;

    public Task addTask(Task task){
        task.setId(UUID.randomUUID().toString().split("-")[0]);
        return repository.save(task);
    }
    public Task getUserId(Task task){
        task.setId(UUID.randomUUID().toString().split("-")[0]);
        return repository.save(task);
    }

    public List<Task> findAllTasks(){
       return repository.findAll();
    }

    public Task getTaskById(String id){
        return repository.findById(id).get();
    }
    public List<Task> getTaskByName(String name){
        return repository.findByName(name);
    }
    public List<Task> getTaskByEmail(String email){
        return repository.getTaskByEmail(email);
    }
    public Task updateTask(Task taskRequest){
       Task existingTask= repository.findById(taskRequest.getId()).get();
        existingTask.setName(taskRequest.getName());
        existingTask.setEmail(taskRequest.getEmail());
        existingTask.setAddress(taskRequest.getAddress());
        existingTask.setFname(taskRequest.getFname());
        existingTask.setTname(taskRequest.getTname());
        existingTask.setStandard(taskRequest.getStandard());
        existingTask.setPercentage(taskRequest.getPercentage());
        existingTask.setPhone(taskRequest.getPhone());
        return repository.save(existingTask);

    }



    /*public String deleteTask(String id){
        repository.deleteById(id);
        return "task deleted from dashboard";

    }*/


    public void deleteStudent(String id) {
        repository.deleteById(id);
    }


    public boolean checkUser(String userId, String password) {
        if (userId.equals("Admin") && password.equals("Admin@123")) {
            return true;
        }
        else{
            return false;
        }
    }
}
